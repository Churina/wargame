package Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GameDemo {
    public static List<Card> allCards = new ArrayList<>();

    static{
        String[] sizes = {"3","4","5","6","7","8","9","10","J","Q","K","A","2"};
        String[] colors = {"Hearts", "Spades", "Clubs", "Diamonds"};

        int index = 0;
        for(String size : sizes){
            index++;
            for(String color : colors){
                Card c = new Card(size,color,index);
                allCards.add(c);
            }
        }

        Card c1 = new Card("","Queen",++index);
        Card c2 = new Card("", "King",++index);
        Collections.addAll(allCards,c1,c2);
        System.out.println(allCards);
    }


    public static void main(String[] args) {
        Collections.shuffle(allCards);
        System.out.println(allCards);

        List<Card> player1 = new ArrayList<>();
        List<Card> player2 = new ArrayList<>();
        List<Card> player3 = new ArrayList<>();

        for (int i = 0; i < allCards.size()-3; i++) {
            Card c = allCards.get(i);
            if(i % 3 == 0){
                player1.add(c);
            } else if (i % 3 == 1){
                player2.add(c);
            } else {
                player3.add(c);
            }
        }

        List<Card> lastThreeCards = allCards.subList(allCards.size()- 3,allCards.size());

//        paixu:
//        Collections.sort(player1, new Comparator<Card>() {
//            @Override
//            public int compare(Card o1, Card o2) {
//                return 0;
//            }
//        })

        
        sortCard(player1);
        sortCard(player2);
        sortCard(player3);
        System.out.println("player1 has:" + player1);
        System.out.println("player2 has:" + player2);
        System.out.println("player3 has:" + player3);
        System.out.println("last cards:" + lastThreeCards);

    }

//    private static void sortCard(List<Card> cards) {
//        Collections.sort(cards, new Comparator<Card>() {
//            @Override
//            public int compare(Card o1, Card o2) {
//                return o2.getIndex() - o1.getIndex();
//            }
//        });
//    }

       private static void sortCard(List<Card> cards) {
        Collections.sort(cards, (o1,  o2) -> o2.getIndex() - o1.getIndex());
    }
}
