public class CardValue {
    //assigns a comparable integer value to the card based on its first Character
    public static int getCardValue(Character cardChar, int cardValue) {
        switch(cardChar){
            case 'A': cardValue = 1; break;
            case '2': cardValue = 2; break;
            case '3': cardValue = 3; break;
            case '4': cardValue = 4; break;
            case '5': cardValue = 5; break;
            case '6': cardValue = 6; break;
            case '7': cardValue = 7; break;
            case '8': cardValue = 8; break;
            case '9': cardValue = 9; break;
            case '1': cardValue = 10; break;
            case 'J': cardValue = 11; break;
            case 'Q': cardValue = 12; break;
            case 'K': cardValue = 13; break;
        }
        return cardValue;
    }
}
